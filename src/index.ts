import { resolve } from "path";
import {
  RuleOutput,
  RuleOutputDecoration,
  loadPlugins,
  loadPresetRules,
  loadRules,
  RulesConfig,
  RuleExports,
} from "./rules";
export { NodeType, NodeTypes } from "./nodeTypes";
import { visitDir } from "./parsers/fs";
import * as filesRule from "./rules/files";

export {
  Rule,
  RuleExports,
  RuleId,
  Level,
  RuleOptions,
  RulesConfig,
  RuleOutput,
  Visitor,
  VisitorMap,
  VisitorContext,
  RuleVisitor,
  CreateVisitors,
  getEnterVisitor,
  getExitVisitor,
} from "./rules";
export { isRootDir, DirNode, isDirNode, FileNode, isFileNode } from "./parsers/fs";

export type LintMyRideOptions = Required<PresetOptions> & {
  repoDir: string;
  ignorePaths: string[];
  plugins: string[];
}

export type PresetId = string;
export interface PresetOptions {
  extends?: string[];
  rules: RulesConfig;
}
export interface PresetExports {
  presetId: PresetId;
  preset: PresetOptions;
}

export type AvailablePresets = PresetExports[];
export type AvailableRules = RuleExports<any>[];

export async function run(options: Partial<LintMyRideOptions> = {}): Promise<Array<RuleOutput & RuleOutputDecoration>> {
  const repoDir = resolve(options.repoDir ?? process.cwd());
  const ignorePaths = options.ignorePaths ?? ["node_modules"];
  const plugins = options.plugins ?? [];

  const [availablePresets, availableRules] = await loadPlugins(plugins);
  const presetRules = options.extends ? loadPresetRules(options.extends, availablePresets) : [];
  const finalConfig: LintMyRideOptions = {
    repoDir: repoDir,
    ignorePaths: ignorePaths,
    plugins: plugins,
    rules: { ...presetRules, ...options.rules },
  } as LintMyRideOptions;

  if (typeof finalConfig.rules !== "object") {
    throw new Error("Please specify Rules to check for.");
  }

  const defaultRules: AvailableRules = [
    filesRule,
  ];

  const rules = loadRules(finalConfig.rules, availableRules.concat(defaultRules));
  const enterOutputs = await visitDir(
    repoDir,
    rules,
    finalConfig,
    'enter',
  );
  const exitOutputs = await visitDir(
    repoDir,
    rules,
    finalConfig,
    'exit',
  );
  return enterOutputs.concat(exitOutputs);
}

export async function runRuleOnFixture<RuleOptions extends any = undefined>(
  rule: RuleExports<any>,
  fixturePath:
  string,
  options?: RuleOptions,
): Promise<Array<RuleOutput & RuleOutputDecoration>> {
  const rulesConfig: RulesConfig = {
    [rule.ruleId]: { level: "error", options: options },
  };
  const rules = loadRules(rulesConfig, [rule]);
  const config: LintMyRideOptions = {
    extends: [],
    ignorePaths: [],
    repoDir: fixturePath,
    rules: rulesConfig,
    plugins: [],
  };
  const enterOutputs = await visitDir(
    fixturePath,
    rules,
    config,
    'enter',
  );
  const exitOutputs = await visitDir(
    fixturePath,
    rules,
    config,
    'exit',
  );
  return enterOutputs.concat(exitOutputs);
}
