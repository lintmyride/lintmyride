import { DirNode, FileNode } from "./parsers/fs";

export type NodeTypes = DirNode | FileNode;

export type NodeType<Type extends string = NodeTypeId<NodeTypes>> = { type: Type };
export type NodeTypeId<N> = N extends NodeType<infer TypeId> ? TypeId : never;
