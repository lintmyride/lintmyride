import { NodeType, NodeTypes, AvailableRules, AvailablePresets } from "./index";
import { NodeTypeId } from "./nodeTypes";

export type RuleId = string;
export type Level = "off" | "warn" | "error";
export type RuleOptions<CustomOptions> = {
  level: Level;
  options?: CustomOptions;
};
/** Map describing which rules are enabled, and optionally passing options to the rules */
export type RulesConfig = Record<RuleId, Level | RuleOptions<any>>;

export type RuleOutput = {
  output: string;
  url?: string;
};
export type RuleOutputDecoration = {
  path: string;
  ruleId: string;
  level: Level;
};

export type Rule<N extends NodeType> = {
  ruleId: string;
  visitors: VisitorMap<N>;
  level: Level;
};

export type VisitorMap<N extends NodeType> = Record<NodeTypeId<N>, RuleVisitor<N>>;
export type Visitor<N extends NodeType> = (node: N) => RuleOutput | void;
export type RuleVisitor<N extends NodeType> = Visitor<N> | Partial<{ enter: Visitor<N>; exit: Visitor<N>; }>;

export type VisitorContext<RuleOptions extends any = undefined> = { options?: RuleOptions };
export type CreateVisitors<N extends NodeType, RuleOptions extends any = undefined>
  = (context: VisitorContext<RuleOptions>) => VisitorMap<N>;

export type RuleExports<N extends NodeType> = {
  ruleId: RuleId;
  createVisitors: CreateVisitors<N, any>;
};

export function loadRules(rulesConfig: RulesConfig, availableRules: AvailableRules): Rule<NodeTypes>[] {
  const rules: Rule<NodeTypes>[] = [];
  const context = {};

  availableRules.forEach(rule => {
    if (typeof rule !== "object") {
      throw new Error(`The Rule \`${rule}\` could not be found. Please check your spelling, or file a bug with the Rule author.`);
    }
    if (typeof rulesConfig[rule.ruleId] === "undefined") {
      return;
    }

    const ruleContext: VisitorContext<unknown> = {
      ...context,
      options: getRuleOptions(rulesConfig[rule.ruleId]),
    };

    rules.push({
      ruleId: rule.ruleId,
      visitors: rule.createVisitors(ruleContext),
      level: getLevel(rulesConfig[rule.ruleId]),
    });
  });

  return rules;
}
export async function loadPlugins(plugins: string[]): Promise<[AvailablePresets, AvailableRules]> {
  const pluginImports = await Promise.all(plugins.map(plugin => import(plugin)));

  let presets: AvailablePresets = [];
  pluginImports.forEach(pluginImport => {
    if (Array.isArray(pluginImport.presets)) {
      presets = [ ...presets, ...pluginImport.presets ];
    }
  });

  let rules: AvailableRules = [];
  pluginImports.forEach(pluginImport => {
    if (Array.isArray(pluginImport.rules)) {
      rules = [ ...rules, ...pluginImport.rules ];
    }
  });

  return [presets, rules];
}
export function loadPresetRules(requestedPresets: string | string[], availablePresets: AvailablePresets): RulesConfig {
  let presetRules: RulesConfig = {};
  requestedPresets = Array.isArray(requestedPresets) ? requestedPresets : [requestedPresets];

  requestedPresets.forEach(requestedPreset => {
    const respondedPreset = availablePresets.find(preset => preset.presetId === requestedPreset);

    if (typeof respondedPreset !== "object") {
      throw new Error(`Preset \`${requestedPreset}\` could not be loaded. Please check your spelling, or report a bug with the preset author.`);
    }

    const recursivelyRequestedPresets = respondedPreset.preset.extends;
    if (typeof recursivelyRequestedPresets !== "undefined") {
      presetRules = {
        ...presetRules,
        ...loadPresetRules(recursivelyRequestedPresets, availablePresets),
      };
    }
    presetRules = { ...presetRules, ...respondedPreset.preset.rules };
  });

  return presetRules;
}
function getLevel(ruleConfig: RulesConfig[keyof RulesConfig]): Level {
  return (typeof ruleConfig === "string") ? ruleConfig : ruleConfig.level;
}

function getRuleOptions(ruleConfig: RulesConfig[keyof RulesConfig]): undefined | unknown {
  return (typeof ruleConfig === "string") ? undefined : ruleConfig.options;
}

export function getEnterVisitor<N extends NodeTypes>(ruleVisitor: RuleVisitor<N>): Visitor<N> {
  if (typeof ruleVisitor === "function") {
    return ruleVisitor;
  }
  if (typeof ruleVisitor === "object" && typeof ruleVisitor.enter === "function") {
    return ruleVisitor.enter;
  }

  return () => undefined;
}

export function getExitVisitor<N extends NodeTypes>(ruleVisitor: RuleVisitor<N>): Visitor<N> {
  if (typeof ruleVisitor === "object" && typeof ruleVisitor.exit === "function") {
    return ruleVisitor.exit;
  }

  return () => undefined;
}
