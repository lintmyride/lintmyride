import { describe, it, jest } from "@jest/globals";
import { getEnterVisitor, getExitVisitor } from "./rules";

describe("getEnterVisitor", () => {
  it("returns it if it is a plain function", () => {
    const mockFn: any = jest.fn();

    const visitor = getEnterVisitor(mockFn);

    expect(visitor).toEqual(mockFn);
  });

  it("returns the enter visitor if it contains it as a property", () => {
    const mockFn: any = jest.fn();

    const visitor = getEnterVisitor({ enter: mockFn });

    expect(visitor).toEqual(mockFn);
  });

  it("returns a no-op function if no visitor was passed", () => {
    const visitor = getEnterVisitor(undefined as any);

    expect(visitor).toBeDefined();
  });
});

describe("getExitVisitor", () => {
  it("returns a no-op function if it is a plain function (=the enter function, not the exit function)", () => {
    const mockFn: any = jest.fn();

    const visitor = getExitVisitor(mockFn);

    expect(visitor).not.toEqual(mockFn);
  });

  it("returns the exit visitor if it contains it as a property", () => {
    const mockFn: any = jest.fn();

    const visitor = getExitVisitor({ exit: mockFn });

    expect(visitor).toEqual(mockFn);
  });

  it("returns the exit visitor if it contains both it and an enter function as a property", () => {
    const mockEnterFn: any = jest.fn();
    const mockExitFn: any = jest.fn();

    const visitor = getExitVisitor({ enter: mockEnterFn, exit: mockExitFn });

    expect(visitor).toEqual(mockExitFn);
  });

  it("does not return the enter visitor if it contains it as a property", () => {
    const mockFn: any = jest.fn();

    const visitor = getExitVisitor({ enter: mockFn });

    expect(visitor).not.toEqual(mockFn);
  });

  it("returns a no-op function if no visitor was passed", () => {
    const visitor = getExitVisitor(undefined as any);

    expect(visitor).toBeDefined();
  });
});
