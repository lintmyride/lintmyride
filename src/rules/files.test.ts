import { describe, it, expect } from "@jest/globals";
import path from "path";
import { DirNode, getEnterVisitor, VisitorContext, runRuleOnFixture } from "../index";
import * as rule from "./files";
import { RuleOptions } from "./files";

describe("general/files", () => {
  const mockContext: VisitorContext<RuleOptions> = { options: {} };

  it("finds issues in the fixture", async () => {
    const outputs = await runRuleOnFixture(rule, path.resolve(__dirname, "../../fixtures/empty-project"));

    expect(outputs).toEqual([
      {
        level: "error",
        output: "Required files not present: README.md.",
        path: "/",
        ruleId: "general/files",
      },
    ]);
  });

  it("allows defining your own list of required files", () => {
    const rootNode: DirNode = {
      type: "Dir",
      path: "",
      name: "",
      children: ["README.md"],
    };

    const visitors = rule.createVisitors({
      ...mockContext,
      options: {
        requiredFiles: ["README", "CHANGELOG"]
      },
    });
    const dirEnterVisitor = getEnterVisitor(visitors.Dir);

    dirEnterVisitor(rootNode);

    expect(dirEnterVisitor(rootNode))
      .toEqual({
        output: "Required files not present: README, CHANGELOG.",
      });
  });

  it("rejects projects that do not include a README by default", () => {
    const rootNode: DirNode = {
      type: "Dir",
      path: "",
      name: "",
      children: [],
    };

    const visitors = rule.createVisitors(mockContext);
    const dirEnterVisitor = getEnterVisitor(visitors.Dir);

    dirEnterVisitor(rootNode);

    expect(dirEnterVisitor(rootNode))
      .toEqual({
        output: "Required files not present: README.md.",
      });
  });

  it("allows a repository that includes a README.md", () => {
    const rootNode: DirNode = {
      type: "Dir",
      path: "",
      name: "",
      children: ["README.md"],
    };


    const visitors = rule.createVisitors(mockContext);
    const dirEnterVisitor = getEnterVisitor(visitors.Dir);

    dirEnterVisitor(rootNode);

    expect(dirEnterVisitor(rootNode)).toBeUndefined();
  });
});
