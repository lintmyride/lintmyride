import { RuleId, CreateVisitors, DirNode } from "../index";
import { isRootDir } from "../parsers/fs";

export const ruleId: RuleId = "general/files";

export interface RuleOptions {
  requiredFiles?: string[];
}

export const createVisitors: CreateVisitors<DirNode, RuleOptions> = ({ options }) => {
  const requiredFiles = options?.requiredFiles ?? ["README.md"];
  return {
    Dir: (node) => {
      // We'll make this configurable later; this is just to test included Rules.
      if (isRootDir(node)) {
        const missingFiles = requiredFiles.filter(file => !node.children.includes(file));

        if(missingFiles.length > 0) {
          return {
            output: `Required files not present: ${missingFiles.join(", ")}.`,
          };
        }
      }
    },
  };
};
