import fs from "fs";
import { resolve, relative, basename } from "path";
import minimatch from "minimatch";
import {
  NodeTypes,
  LintMyRideOptions,
  NodeType,
  getEnterVisitor,
  getExitVisitor,
} from "../index";
import {
  Rule,
  RuleOutput,
  RuleOutputDecoration
} from "../rules";

export async function visitDir(
  dir: string,
  rules: Rule<NodeTypes>[],
  options: LintMyRideOptions,
  direction: 'enter' | 'exit',
): Promise<Array<RuleOutput & RuleOutputDecoration>> {
  const children = await fs.promises.readdir(dir);

  const node: DirNode = {
    type: 'Dir',
    path: relative(options.repoDir, dir),
    name: basename(dir),
    children: children,
  };

  const outputs: Array<RuleOutput & RuleOutputDecoration> = [];

  if (direction === 'enter') {
    for (let rule of rules) {
      if (typeof rule.visitors.Dir === "undefined") {
        continue;
      }
      const enterVisitor = getEnterVisitor(rule.visitors.Dir);
      const output = enterVisitor(node);
      if (output) {
        outputs.push({
          ...output,
          path: "/" + node.path,
          ruleId: rule.ruleId,
          level: rule.level,
        });
      }
    }
  }

  for (const child of children) {
    const childPath = resolve(dir, child);
    if (options.ignorePaths.some((ignoreGlob) => minimatch(relative(options.repoDir, childPath), ignoreGlob))) {
      continue;
    }

    const fileStat = await fs.promises.stat(childPath);
    if (fileStat.isDirectory()) {
      try {
        const childOutputs = await visitDir(childPath, rules, options, direction);
        outputs.push(...childOutputs);
      }
      catch {
        // We simply ignore directories we couldn't read, e.g. that we don't have access to.
      }
    }
    else if (fileStat.isFile()) {
      try {
        const childOutputs = await visitFile(childPath, rules, options, direction);
        outputs.push(...childOutputs);
      }
      catch {
        // We simply ignore files we couldn't read, e.g. that we don't have access to.
      }
    }
  }

  if (direction === 'exit') {
    for (let rule of rules) {
      if (typeof rule.visitors.Dir === "undefined") {
        continue;
      }
      const enterVisitor = getExitVisitor(rule.visitors.Dir);
      const output = enterVisitor(node);
      if (output) {
        outputs.push({
          ...output,
          path: "/" + node.path,
          ruleId: rule.ruleId,
          level: rule.level,
        });
      }
    }
  }

  return outputs;
}

export async function visitFile(
  filePath: string,
  rules: Rule<FileNode>[],
  options: LintMyRideOptions,
  direction: 'enter' | 'exit'
): Promise<Array<RuleOutput & RuleOutputDecoration>> {
  const contents = await fs.promises.readFile(filePath, 'utf-8');

  const node: FileNode = {
    type: 'File',
    path: relative(options.repoDir, filePath),
    name: basename(filePath),
    contents: contents,
  };

  const outputs: Array<RuleOutput & RuleOutputDecoration> = [];

  for (let rule of rules) {
    if (typeof rule.visitors.File === "undefined") {
      continue;
    }
    const visitor = direction === 'enter' ? getEnterVisitor(rule.visitors.File) : getExitVisitor(rule.visitors.File);
    const output = visitor(node);
    if (output) {
      outputs.push({
        ...output,
        path: "/" + node.path,
        ruleId: rule.ruleId,
        level: rule.level,
      });
    }
  }

  return outputs;
}

export type DirNode = NodeType<'Dir'> & {
  path: string,
  name: string,
  children: string[],
};
export type FileNode = NodeType<'File'> & {
  path: string,
  name: string,
  contents: string,
};

export function isDirNode(node: NodeTypes): node is DirNode {
  return node.type === "Dir";
}

export function isFileNode(node: NodeTypes): node is FileNode {
  return node.type === "File";
}

export function isRootDir(node: DirNode) {
  return node.path === "";
}
