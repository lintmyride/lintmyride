#!/usr/bin/env node
import { program } from "commander";
import fs from "fs";
import chalk from "chalk";
import { run, LintMyRideOptions } from "./index";
import { RuleOutput, RuleOutputDecoration } from "./rules";

program
  .option("--config <path>", "Location of your lintmyride.json config file.")
  .arguments("[targetDir]")
  .action(async (targetDir?: string) => {
    const config = await loadConfig(program.config ?? "lintmyride.json");
    if (typeof targetDir === "string") {
      config.repoDir = targetDir;
    }
    run(config).then(outputs => {
      if (outputs.length === 0) {
        console.log("Lint successful.");
        process.exit(0);
      }

      const nrErrors = outputs.filter(output => output.level === "error").length;
      const nrWarns = outputs.filter(output => output.level === "warn").length;

      console.log(chalk.bold(`${outputs.length} issues found:`));
      outputs.forEach(logOutput);
      console.log(chalk.dim(`${nrErrors} error(s), ${nrWarns} warning(s).`));

      process.exit(nrErrors);
    });
  });


program.parse(process.argv);

async function loadConfig(path: string): Promise<Partial<LintMyRideOptions>> {
  const stat = await fs.promises.stat(path);
  if (!stat.isFile()) {
    return {};
  }
  const contents = await fs.promises.readFile(path, "utf-8");
  return JSON.parse(contents);
}

function logOutput(output: RuleOutput & RuleOutputDecoration) {
  const icon = output.level === "error" ? chalk.red("❌️") : chalk.yellow("❗️");

  console.log(`${icon.padEnd(1)} ${chalk.dim(output.path)}`);
  console.log(`   ${output.output} ${chalk.underline.dim(`[${output.ruleId}]`)}`);
  if (typeof output.url === "string") {
    console.log(`   🔗 ${output.url}`);
  }
}