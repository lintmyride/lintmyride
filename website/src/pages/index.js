import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: <>Catch common errors</>,
    imageUrl: 'img/undraw_QA_engineers_dg5p.svg',
    description: (
      <>
        Forgot to check in your lockfile, accidentally used the wrong package manager, or
        erroneously checked in your dependencies? We've all been there.
        But forget about the pitfalls of setting up a project — Lint My Ride won't.
      </>
    ),
  },
  {
    title: <>I heard you like linting&hellip;</>,
    imageUrl: 'img/undraw_fast_car_p4cu.svg',
    description: (
      <>
        Align your code quality tools across your projects: build setup, linter configuration,
        inclusion of copyright notices&hellip; You name it!
      </>
    ),
  },
  {
    title: <>Of course, open source</>,
    imageUrl: 'img/undraw_open_source_1qxw.svg',
    description: (
      <>
        Lint My Ride is for everyone.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Home`}
      description={siteConfig.description}>
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--link button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/use/getting-started')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
