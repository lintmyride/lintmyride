---
id: plugins
title: Writing plugins
---

A plugin is an npm package that provides [new rules](./rules), [presets](../use/presets), or both.

They do so by exporting an array named `rules` and/or one named `presets` from their
[`main`](https://docs.npmjs.com/files/package.json#main) module, respectively. And that's all there
is to it, really!
