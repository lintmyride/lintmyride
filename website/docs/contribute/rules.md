---
id: rules
title: Writing rules
---

An example rule is below, followed by a more detailed explanation:

```typescript
import { RuleId, CreateVisitors, DirNode } from "lintmyride";

export const ruleId: RuleId = "js/lockfile";

export const createVisitors: CreateVisitors<DirNode> = (context) => {
  return {
    Dir: (node) => {
      if (
        node.path === "" &&
        node.children.includes("package.json") &&
        !node.children.includes("package-lock.json") &&
        !node.children.includes("yarn.lock")
      ) {
        return {
          output: "Node projects should include a lock file.",
        };
      }
    },
  };
};

```

A _rule_ is a file that exports two variables:

- `ruleId`
- `createVisitors`

The first one, `ruleId`, is simply a string that is used to identify the rule. This should be
prefixed by the identifier of the plugin the rule is part of. For example, the rule ID of the rule
checking whether an npm project contains a lockfile is `"js/lockfile"`.

The latter, `createVisitors`, is the meat of the rule. This is a function that returns an object
defining _visitors_: functions that receive an object describing an element in the repository (a
_node_), such as a file or a directory, and return either `undefined` — if the node did not
violate the rule — or an object describing the violation.

## Visitor output

A visitor returns `undefined` if the visited node does not violate the relevant rule. In case of a
violation, however, a visitor can return an object with the following properties:

- `output`: A description of the violation.
- `url`: (optional) link to a web page that contains more information about the violation and how to
  resolve it.

## Node types

Currently, Lint My Ride supports two types of nodes that can be visited: a _file_ node or a
_directory_ node.

A **directory node** is passed to `Dir` visitors, and contains the following properties:
- `path`: the path to the given directory, relative to the project root. It does not start with a
  `/`.
- `name`: the name of the given directory.
- `children`: array containing the file/directory names of the direct children of the directory.


A **file node** is passed to `File` visitors, and contains the following properties:
- `path`: the path to the given file, relative to the project root. It does not start with a
  `/`.
- `name`: the name of the given file.
- `contents`: string containing the contents of the file. (This may be removed in future versions.)

## "Enter" and "exit" visitors

While a visitor as described above will allow you to return linting errors based on a single file's
or directory's contents, sometimes a linting rule is dependent on several files. For example, you
may want to verify that a repository that has a `package.json` at its root also lists `node_modules`
in its `.gitignore`.

To enable this, you can define _enter_ and _exit_ visitors. Lint My Ride calls the enter visitors
once for every node, and then calls the exit visitors for those same nodes in reverse order. Enter
and exit visitors can be defined by returning an object with an `enter` and `exit` key, each
containing the respective visitor, instead of the original visitor.

This allows us to create our example as follows:

```typescript
import { CreateVisitors, RuleId, FileNode  } from "lintmyride";

export const ruleId: RuleId = "js/gitignore";

export const createVisitors: CreateVisitors<FileNode> = (context) => {
  let topLevelPackageJson: boolean = false;

  return {
    File: {
      enter: (node) => {
        if (node.name === "package.json" && node.path === "") {
          topLevelPackageJson = true;
        }
      },
      exit: (node) => {
        if (!topLevelPackageJson || node.name !== ".gitignore" || node.path !== "") {
          return;
        }
        const nodeModulesLineRegex = /^node_modules$/gm;
        if(!nodeModulesLineRegex.test(node.contents)) {
          return {
            output: "node_modules should not be committed, and hence should be listed in .gitignore."
          };
        }
      },
    },
  };
};
```
