---
id: configuration
title: Configuration
---

Configuration is done in a file `lintmyride.json` at the root of your project. An example
configuration is below, followed by a reference listing every possible option.

:::caution
This section is not complete yet. Contributions welcome!
:::

```json
{
  "plugins": ["@lintmyride/preset-plugin", "@lintmyride/plugin-js"],
  "extends": ["lintmyride-plugin"]
}
```

## `plugins`

**Default value:** `[]`

A list of the plugins you want to use. Only installed plugins are available.

## `extends`

**Default value:** `[]`

If you want to re-use your configuration across different projects, you can package them as a
preset. Presets you want to 
