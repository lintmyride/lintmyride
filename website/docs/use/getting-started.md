---
id: getting-started
title: Getting started
---

To get started with Lint My Ride, create a file `lintmyride.json` at the root of your repository in
which you can specify which rules to enable.

For example, for a JavaScript project you'd use `@lintmyride/plugin-js`:

```json title="lintmyride.json"
{
  "plugins": ["@lintmyride/plugin-js"],
  "extends": "js/recommended"
}
```

Then, in an environment with [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/)
available (e.g. your build system), run:

```bash
npm install lintmyride @lintmyride/plugin-js
npx lintmyride
```

Lint My Ride will then terminate with exit code 0 if your repository satisfied all linting rules, or
output what rules were not met.
