---
id: introducing-lintmyride
title: Introducing Lint My Ride
author: Vincent Tunru
author_title: Lint My Ride contributor
author_url: https://VincentTunru.com
author_image_url: /img/avatars/vincent.jpg
tags: []
---

This is just a heads-up that Lint My Ride is now live! Well, technically then. Alright, this is mostly
just filler content so that there is a blog up.

But Lint My Ride works, and rules can be written for it! There aren't many now, but if you're interested
in writing your own, it should be good to go.
