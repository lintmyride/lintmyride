module.exports = {
  guide: [
    {
      type: 'category',
      label: 'Using Lint My Ride',
      items: ['use/getting-started', 'use/configuration', 'use/presets'],
    },
    {
      type: 'category',
      label: 'Contributing',
      items: ['contribute/plugins', 'contribute/rules'],
    }
  ],
};
