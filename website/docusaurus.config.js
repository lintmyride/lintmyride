const pkg = require('../package.json');

module.exports = {
  title: 'Lint My Ride',
  tagline: pkg.description,
  url: 'https://lintmyride.vincenttunru.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  // organizationName: 'facebook', // Usually your GitHub org/user name.
  // projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Lint My Ride',
      logo: {
        alt: '[]',
        src: 'img/wordmark.svg',
      },
      items: [
        {
          to: 'docs/use/getting-started',
          activeBasePath: 'docs/use',
          label: 'Use',
          position: 'left',
        },
        {
          to: 'docs/contribute/plugins',
          activeBasePath: 'docs/contribute',
          label: 'Contribute',
          position: 'left',
        },
        // {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/lintmyride/',
          label: 'Source code',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Using Lint My Ride',
              to: 'docs/use/getting-started',
            },
            {
              label: 'Contributing',
              to: 'docs/contribute/plugins',
            },
          ],
        },
        // {
        //   title: 'Community',
        //   items: [
        //     {
        //       label: 'Stack Overflow',
        //       href: 'https://stackoverflow.com/questions/tagged/lintmyride',
        //     },
        //   ],
        // },
        {
          title: 'More',
          items: [
            {
              label: 'Author',
              href: 'https://VincentTunru.com',
            },
            {
              label: 'Source code',
              href: 'https://gitlab.com/lintmyride/',
            },
          ],
        },
      ],
      copyright: `Copyright © Vincent Tunru`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          homePageId: 'getting-started',
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/lintmyride/lintmyride/-/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/lintmyride/lintmyride/-/edit/master/website/blog/',
          feedOptions: {
            type: 'atom',
            title: 'Lint My Ride blog',
            copyright: 'Copyright © Vincent Tunru',
            language: 'en-GB',
          },
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
